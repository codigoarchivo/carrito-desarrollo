import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { startGoogleLogin, startLoginEmailPassword } from "../../actions/auth";
import { useForm } from "../../hooks/useForm";

const LoginScreen = () => {
  const dispatch = useDispatch();
  const { loading } = useSelector(({ ui }) => ui);

  const [values, handleInputChange] = useForm({
    email: "ana_maria@gmail.com",
    password: "123456",
  });
  const { email, password } = values;
  const handleLogin = (e) => {
    e.preventDefault();
    dispatch(startLoginEmailPassword(email, password));
  };

  const handleGooglelogin = () => {
    dispatch(startGoogleLogin());
  };

  return (
    <>
      <h3 className="auth__main-title">Login</h3>
      <form
        onSubmit={handleLogin}
        className="auth__form animate__animated animate__fadeIn animate__faster"
      >
        <input
          className="auth__form-input"
          type="text"
          name="email"
          placeholder="Email"
          autoComplete="off"
          value={email}
          onChange={handleInputChange}
        />
        <input
          className="auth__form-input"
          type="password"
          name="password"
          placeholder="Password"
          value={password}
          onChange={handleInputChange}
        />
        <button
          className="btn btn-primary btn-block"
          type="submit"
          disabled={loading}
        >
          Login
        </button>

        <div className="auth__form-social">
          <p>Login with social networks </p>
        </div>
        <div className="google" onClick={handleGooglelogin}>
          <div className="google__icon">
            <img
              className="google__icon-img"
              src="https://upload.wikimedia.org/wikipedia/commons/5/53/Google_%22G%22_Logo.svg"
              alt="google button"
            />
          </div>
          <p className="google__text">
            <b>Sign in with google</b>
          </p>
        </div>
        <Link className="links" to="/auth/register">
          Create new account
        </Link>
      </form>
    </>
  );
};

export default LoginScreen;
