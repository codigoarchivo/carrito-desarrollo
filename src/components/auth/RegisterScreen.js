import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { useForm } from "../../hooks/useForm";
import validator from "validator";
import { removeError, setError } from "../../actions/ui";
import { startRegisterWithNameEmailPassword } from "../../actions/auth";

const RegisterScreen = () => {
  const dispatch = useDispatch();
  const { msgError } = useSelector(({ ui }) => ui);

  const [values, handleInputChange] = useForm({
    name: "ANA MARÍA",
    email: "ana_maria@gmail.com",
    password: "123456",
    password2: "123456",
  });
  const { name, email, password, password2 } = values;

  const handleRegister = (e) => {
    e.preventDefault();
    if (inFormValid()) {
      dispatch(startRegisterWithNameEmailPassword(email, password, name));
    }
  };

  const inFormValid = () => {
    if (name.trim().length === 0) {
      dispatch(setError("Name is required"));
      return false;
    } else if (!validator.isEmail(email)) {
      dispatch(setError("Email is not value"));
      return false;
    } else if (password !== password2 || password.length < 5) {
      dispatch(setError("Email is not value"));
      return false;
    }
    dispatch(removeError());
    return true;
  };

  return (
    <>
      <h3 className="auth__main-title">Register</h3>
      <form
        onSubmit={handleRegister}
        className="auth__form animate__animated animate__fadeIn animate__faster"
      >
        {msgError && <div className="auth__alert-error">{msgError}</div>}

        <input
          className="auth__form-input"
          type="text"
          name="name"
          placeholder="Name"
          autoComplete="off"
          value={name}
          onChange={handleInputChange}
        />
        <input
          className="auth__form-input"
          type="text"
          name="email"
          placeholder="Email"
          autoComplete="off"
          value={email}
          onChange={handleInputChange}
        />
        <input
          className="auth__form-input"
          type="password"
          name="password"
          placeholder="Password"
          value={password}
          onChange={handleInputChange}
        />
        <input
          className="auth__form-input"
          type="password"
          name="password2"
          placeholder="Confirm Password"
          value={password2}
          onChange={handleInputChange}
        />
        <button
          className="btn btn-primary btn-block mb-5"
          type="submit"
          // disabled={true}
        >
          Register
        </button>
        <Link className="links " to="/auth/login">
          Already registered?
        </Link>
      </form>
    </>
  );
};

export default RegisterScreen;
