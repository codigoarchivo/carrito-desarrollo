import React from "react";
const Navlist = () => {
  return (
    <>
      <nav className="nav2">
        <ul>
          <li>Bebidas</li>
          <li className="ocultar">Comidas</li>
          <li className="ocultar">Reposteria y panaderia</li>
          <li className="ocultar">Redigerados</li>
          <li className="ocultar">
            <i className="fas fa-th"></i>
          </li>
        </ul>
      </nav>
    </>
  );
};

export default Navlist;
