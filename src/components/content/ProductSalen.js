import React from "react";
import { useSelector } from "react-redux";
import {
  decrementCart,
  incrementCart,
  removeFromCart,
} from "../../actions/cart";
import { useDispatch } from "react-redux";


const ProductSalen = () => {
  const state = useSelector(({ cart }) => cart);
  const dispatch = useDispatch();
  let total = 0;
  state.map((x) => (total += x.inicio * x.incOrde));
  return (
    <>
      <div className="product">
        <table>
          <thead>
            <tr>
              <th>Cantidad</th>
              <th>Producto</th>
              <th>Tax</th>
              <th>Total</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {state.map((x, key) => (
              <tr key={key}>
                <td className="cantidad">
                  <span>{x.incOrde}</span>
                  <img
                    onClick={() => dispatch(incrementCart(x.id))}
                    src={"./assets/más.png"}
                    alt="foto"
                  />
                  <img
                    onClick={() => dispatch(decrementCart(x.id))}
                    src={"./assets/menos.png"}
                    alt="foto"
                  />
                </td>
                <td>{x.nombre}</td>
                <td>{x.precio}</td>
                <td>{x.inicio * x.incOrde}$</td>
                <td onClick={() => dispatch(removeFromCart(x.id))}>
                  <i className="fas fa-trash-alt"></i>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        <hr />
        <div className="totales">
          <p>Total</p>
          <p>{total}$</p>
        </div>

        <div className="desVisit">
          <div className="desVisit__cont">
            <img src={"./assets/empleado.png"} alt="foto" />
            <p>
              Descuento <span>empledo</span>{" "}
            </p>
          </div>
          <div className="desVisit__cont">
            <img src={"./assets/lista.png"} alt="foto" />
            <p>
              Vista <span>cliente</span>{" "}
            </p>
          </div>
        </div>
        <div className="codigo">
          <img src={"./assets/descuento.png"} alt="foto" />
          <p>Codigo de Promoción</p>
        </div>
      </div>
    </>
  );
};

export default ProductSalen;
