import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { pagoCart } from "../../actions/cart";
import Swal from "sweetalert2";

const FormasPago = () => {
  const dispatch = useDispatch();
  const cart = useSelector(({ cart }) => cart);
  const handleSale = () => {
    Swal.fire({
      title: "Estas seguro?",
      text: "No podrás revertir esto!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#007383",
      cancelButtonColor: "#af444e",
      confirmButtonText: "Aceptar!",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire("Procesado!", "Si, Gracias por su Compra.", "success");
        dispatch(pagoCart());
      }
    });
  };
  return (
    <>
      <div className="forma">
        <ul>
          <li>
            <img src={"./assets/pngwing.com.png"} alt="foto" />
          </li>
          <li>
            <img src={"./assets/visa.png"} alt="foto" />
          </li>
          <li>
            <img src={"./assets/mastercard.png"} alt="foto" />
          </li>
          <li>
            <img src={"./assets/uber-eats.jpg"} alt="foto" />
          </li>
          <li>
            <img src={"./assets/tarjeta.png"} alt="foto" />
          </li>
          <li>
            <img src={"./assets/suma.png"} alt="foto" />
          </li>
        </ul>
        <div
          onClick={!!cart[0] ? handleSale : undefined}
          className="forma__pago"
        >
          Pagar
        </div>
      </div>
    </>
  );
};

export default FormasPago;
