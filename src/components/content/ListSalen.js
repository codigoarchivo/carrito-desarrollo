import React from "react";
import Navlist from "./Navlist";
import { store } from "../../data/store";
import FormasPago from "./FormasPago";
import { useDispatch } from "react-redux";
import { addToCart } from "../../actions/cart";
const ListSalen = () => {
  const dispatch = useDispatch();
  const handleCart = (valor) => {
    dispatch(addToCart(valor));
  };

  return (
    <>
      <div className="listContent">
        <Navlist />
        <div className="listproduct">
          <ul>
            {store.map((x, key) => (
              <li key={x.id} onClick={() => handleCart(x)}>
                <img src={`./assets/${x.image}.jpg`} alt="foto" />
                {x.nombre && <h4>{x.nombre}</h4>}
                {x.precio && <pre>{x.precio}</pre>}
              </li>
            ))}
          </ul>
        </div>
        <FormasPago />
      </div>
    </>
  );
};

export default ListSalen;
