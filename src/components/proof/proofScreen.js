import React from "react";
import NavBar from "./NavBar";
import Sidebar from "./Sidebar";
const JournalScreen = () => {
  return (
    <>
      <div>
        <NavBar />
        <Sidebar />
      </div>
    </>
  );
};

export default JournalScreen;
