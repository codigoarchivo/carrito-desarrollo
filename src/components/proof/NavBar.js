import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { startLogout } from "../../actions/auth";
const NavBar = () => {
  const dispatch = useDispatch();
  const { name } = useSelector(({ auth }) => auth);
  const handleLogout = () => {
    dispatch(startLogout());
  };
  return (
    <>
      <nav className="proof__navBar">
        <ul>
          <li>
            <img src={"./assets/market.png"} alt="imagen" />
            <button onClick={handleLogout} className="btn">
              logout
            </button>
          </li>
          <li>
            <p>
              {name} <span>POS</span>
            </p>
            <div className="proof__navBar-cont">
              <img className="proof__navBar-img2" src={"./assets/perfil.png"} alt="imagen" />
            </div>
            <div className="proof__navBar-cont2">
              <i className="fas fa-ellipsis-v"></i>
            </div>
          </li>
        </ul>
      </nav>
    </>
  );
};

export default NavBar;
