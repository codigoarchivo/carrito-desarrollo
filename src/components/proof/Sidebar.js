import React from "react";
import ContentScreen from "./ContentScreen";

const Sidebar = () => {
  return (
    <>
      <ContentScreen/>
      <aside className="proof__sidebar">
        <div className="proof__sidebar-navbar">
          <i className="fas fa-bars"></i>
        </div>
      </aside>
    </>
  );
};

export default Sidebar;
