import React from "react";
import ListSalen from "../content/ListSalen";
import ProductSalen from "../content/ProductSalen";

const ContentScreen = () => {
  return (
    <>
      <div className="position">
        <div className="position__content">
          <ProductSalen />
          <ListSalen />
        </div>
      </div>
    </>
  );
};

export default ContentScreen;
