export const types = {
  login: "[Auth]login",
  logout: "[Auth]logout",

  addcart: "[CART] ADD",
  removecart: "[CART] REMOVE",

  increment: "[IN] INCREMENT",
  decrement: "[DE] DECREMENT",

  pagoCart: "[PAGO] PAGAR",

  uiSetError: "[UI] Set Error",
  uiRemoveError: "[UI] Remove Error",

  uiStartLoading: "[UI] Start loading",
  uiFinishLoading: "[UI] Finish loading",

  notesAddNew: "[Notes] New notes",
  notesActive: "[Notes] Set active note",
  notesLoad: "[Notes] Load notes",
  notesUpdated: "[Notes] Updated note",
  notesFileUrl: "[Notes] Updated image url",
  notesDelete: "[Notes] Delete notes",
  notesAuthCleaning: "[Notes] Logout Cleaning",
};
