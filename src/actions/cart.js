import { types } from "../types";
export function addToCart(item) {
  return {
    type: types.addcart,
    item: item,
  };
}

export function removeFromCart(item) {
  return {
    type: types.removecart,
    id: item,
  };
}
export function incrementCart(item) {
  return {
    type: types.increment,
    id: item,
  };
}
export function decrementCart(item) {
  return {
    type: types.decrement,
    id: item,
  };
}
export function pagoCart() {
  return {
    type: types.pagoCart,
  };
}
