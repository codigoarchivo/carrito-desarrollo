import firebase from "firebase/compat/app";
import "firebase/compat/auth";
import "firebase/compat/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyAukk26SgOI7QucsQLzc7sSVxfivhTsafs",
  authDomain: "react-app--project.firebaseapp.com",
  projectId: "react-app--project",
  storageBucket: "react-app--project.appspot.com",
  messagingSenderId: "875978177364",
  appId: "1:875978177364:web:dd4b6e46e0d771cb7766c1",
};
const firebaseConfigTesting = {
  apiKey: "AIzaSyBk10JOeaCzw19HFUSiAPJ1wfgeNVM9Cbw",
  authDomain: "redux-demo-582a7.firebaseapp.com",
  projectId: "redux-demo-582a7",
  storageBucket: "redux-demo-582a7.appspot.com",
  messagingSenderId: "133532771481",
  appId: "1:133532771481:web:addb5713768d9ab3edc408",
};

if (process.env.NODE_ENV === "test") {
  // test
  firebase.initializeApp(firebaseConfigTesting);
} else {
  // prod
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
}

const db = firebase.firestore();

const googleAuthProvider = new firebase.auth.GoogleAuthProvider();

export { db, googleAuthProvider, firebase };
