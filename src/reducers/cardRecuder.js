import { types } from "../types/index";
export const initialState = [];

export const cart = (state = initialState, action) => {
  switch (action.type) {
    case types.addcart:
      const dupe = state.find((obj) => obj.id === action.item.id);
      return dupe ? state : [...state, action.item];

    case types.removecart:
      return [...state.filter((stat) => stat.id !== action.id)];

    case types.increment:
      return [
        ...state.map((stat) =>
          action.id === stat.id ? { ...stat, incOrde: stat.incOrde + 1 } : stat
        ),
      ];

    case types.decrement:
      return [
        ...state.map((stat) =>
          action.id === stat.id ? { ...stat, incOrde: stat.incOrde - 1 } : stat
        ),
      ];
    case types.pagoCart:
      return [];
    default:
      return state;
  }
};
