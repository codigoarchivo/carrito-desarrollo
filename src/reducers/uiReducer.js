import { types } from "../types/index";

const initialState = {
  loading: false,
  msgError: null,
};
export const uiReducer = (states = initialState, action) => {
  switch (action.type) {
    case types.uiSetError:
      return {
        ...states,
        msgError: action.payload,
      };
    case types.uiRemoveError:
      return {
        ...states,
        msgError: null,
      };
    case types.uiStartLoading:
      return {
        ...states,
        loading: true,
      };
    case types.uiFinishLoading:
      return {
        ...states,
        loading: false,
      };
    default:
      return states;
  }
};
