import React from "react";
import { BrowserRouter as Router, Switch, Redirect } from "react-router-dom";

import { useDispatch } from "react-redux";
import { firebase } from "../firebase/firebase-config";

import AuthRouter from "./AuthRouter";
import JournalScreen from "../components/proof/proofScreen";

import { login } from "../actions/auth";
import { PrivateRoute } from "./PrivateRoute";
import { PublicRoute } from "./PublicRoute";
import { startLoadingNotes } from "../actions/notes";

const AppRouter = () => {
  const dispatch = useDispatch();

  const [checking, setChecking] = React.useState(true);
  const [isloggedIn, setIsloggedIn] = React.useState(false);

  React.useEffect(() => {
    firebase.auth().onAuthStateChanged((user) => {
      if (user?.uid) {
        dispatch(login(user.uid, user.displayName));
        setIsloggedIn(true);
        dispatch(startLoadingNotes(user.uid));
      } else {
        setIsloggedIn(false);
      }
      setChecking(false);
    });
  }, [dispatch, setChecking, setIsloggedIn]);

  if (checking) {
    return (
      <h1
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
      >
        Wait...
      </h1>
    );
  }
  return (
    <Router>
      <Switch>
        <PublicRoute
          path="/auth"
          isAutenticate={isloggedIn}
          component={AuthRouter}
        />
        <PrivateRoute
          exact
          path="/"
          isAutenticate={isloggedIn}
          component={JournalScreen}
        />
        <Redirect to="/auth/login" />
      </Switch>
    </Router>
  );
};

export default AppRouter;
