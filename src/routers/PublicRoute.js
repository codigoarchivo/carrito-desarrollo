import React from "react";
import { Redirect, Route } from "react-router";
import PropTypes from "prop-types";

export const PublicRoute = ({
  isAutenticate,
  component: Component,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      component={(props) =>
        isAutenticate ? <Redirect to="/" /> : <Component {...props} />
      }
    />
  );
};
PublicRoute.propTypes = {
  isAutenticate: PropTypes.bool.isRequired,
  component: PropTypes.func.isRequired,
};
