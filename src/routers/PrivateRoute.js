import React from "react";
import { Redirect, Route } from "react-router";
import PropTypes from "prop-types";

export const PrivateRoute = ({
  isAutenticate,
  component: Component,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      component={(props) =>
        isAutenticate ? <Component {...props} /> : <Redirect to="/auth/login" />
      }
    />
  );
};
PrivateRoute.propTypes = {
  isAutenticate: PropTypes.bool.isRequired,
  component: PropTypes.func.isRequired,
};
